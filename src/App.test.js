import React from 'react';
import { render, cleanup } from '@testing-library/react';
import Counter from './Counter';

describe('Counter', () => {
  afterEach(cleanup);

  it('should take a snapshot', () => {
    const { asFragment } = render(<Counter />);

    expect(asFragment(<Counter />)).toMatchSnapshot();
  });
});
